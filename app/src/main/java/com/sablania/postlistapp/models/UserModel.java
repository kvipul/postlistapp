package com.sablania.postlistapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ViPul Sublaniya on 10-06-2018.
 */

public class UserModel implements Serializable {
    @SerializedName("thumbnailUrl")
    private String thumbnailUrl;

    @SerializedName("firstName")
    private String firstName;

    @SerializedName("lastName")
    private String lastName;

    @SerializedName("authorName")
    private String authorName;

    @SerializedName("uuid")
    private String uuid;

    @SerializedName("designation")
    private String designation;

    @SerializedName("joinedSince")
    private String joinedSince;

    @SerializedName("followed")
    private boolean followed;

    @SerializedName("editor")
    private boolean editor;

    @SerializedName("loggedIn")
    private boolean loggedIn;

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getUuid() {
        return uuid;
    }

    public String getDesignation() {
        return designation;
    }

    public String getJoinedSince() {
        return joinedSince;
    }

    public boolean isFollowed() {
        return followed;
    }

    public boolean isEditor() {
        return editor;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }
}
