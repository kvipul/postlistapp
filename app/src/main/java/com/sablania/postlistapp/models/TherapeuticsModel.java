package com.sablania.postlistapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ViPul Sublaniya on 10-06-2018.
 */

public class TherapeuticsModel implements Serializable {
    @SerializedName("name")
    private String name;

    @SerializedName("uuid")
    private String uuid;

    public String getName() {
        return name;
    }

    public String getUuid() {
        return uuid;
    }
}
