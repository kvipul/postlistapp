package com.sablania.postlistapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ViPul Sublaniya on 10-06-2018.
 */

public class PostModel implements Serializable {
    @SerializedName("title")
    private String title;

    @SerializedName("uuid")
    private String uuid;

    @SerializedName("postType")
    private int postType;

    @SerializedName("typeName")
    private String typeName;

    @SerializedName("featuredAttachmentUrl")
    private String featuredAttachmentUrl;

    @SerializedName("featuredAttachmentWidth")
    private String featuredAttachmentWidth;

    @SerializedName("featuredAttachmentHeigth")
    private String featuredAttachmentHeigth;

    @SerializedName("embedURL")
    private String embedURL;

    @SerializedName("creationDate")
    private String creationDate;

    @SerializedName("answerCount")
    private int answerCount;

    @SerializedName("viewCount")
    private int viewCount;

    @SerializedName("voteCount")
    private int voteCount;

    @SerializedName("user")
    private UserModel user;

    @SerializedName("shortBodyText")
    private String shortBodyText;

    @SerializedName("deleted")
    private boolean deleted;

    @SerializedName("therapeutics")
    private List<TherapeuticsModel> therapeutics;

    @SerializedName("attachments")
    private List<AttachmentModel> attachments;

    @SerializedName("infocenter")
    private boolean infocenter;

    @SerializedName("viewed")
    private boolean viewed;

    @SerializedName("voted")
    private boolean voted;

    public String getTitle() {
        return title;
    }

    public String getUuid() {
        return uuid;
    }

    public int getPostType() {
        return postType;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getFeaturedAttachmentUrl() {
        return featuredAttachmentUrl;
    }

    public String getFeaturedAttachmentWidth() {
        return featuredAttachmentWidth;
    }

    public String getFeaturedAttachmentHeigth() {
        return featuredAttachmentHeigth;
    }

    public String getEmbedURL() {
        return embedURL;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public int getAnswerCount() {
        return answerCount;
    }

    public int getViewCount() {
        return viewCount;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public UserModel getUser() {
        return user;
    }

    public String getShortBodyText() {
        return shortBodyText;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public List<TherapeuticsModel> getTherapeutics() {
        return therapeutics;
    }

    public List<AttachmentModel> getAttachments() {
        return attachments;
    }

    public boolean isInfocenter() {
        return infocenter;
    }

    public boolean isViewed() {
        return viewed;
    }

    public boolean isVoted() {
        return voted;
    }
}

/**
 * what is post type
 */
