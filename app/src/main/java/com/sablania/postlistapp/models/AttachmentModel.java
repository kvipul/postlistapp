package com.sablania.postlistapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ViPul Sublaniya on 10-06-2018.
 */

public class AttachmentModel implements Serializable {
    @SerializedName("url")
    private String url;

    @SerializedName("name")
    private String name;

    @SerializedName("uuid")
    private String uuid;

    @SerializedName("width")
    private int width;

    @SerializedName("height")
    private int height;

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public String getUuid() {
        return uuid;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
