package com.sablania.postlistapp.utils;

import android.support.v7.widget.RecyclerView;
import android.view.animation.OvershootInterpolator;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;

/**
 * Created by ViPul Sublaniya on 10-06-2018.
 */

//class for utility functions
public class Constants {
    public static RecyclerView.Adapter addAnimationToAdapter(RecyclerView.Adapter adapter) {
        int animationDuration = 700;
        ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
        scaleInAnimationAdapter.setFirstOnly(false);
        scaleInAnimationAdapter.setDuration(animationDuration);
        scaleInAnimationAdapter.setInterpolator(new OvershootInterpolator());

        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(scaleInAnimationAdapter);
//        alphaAdapter.setDuration(animationDuration);
        alphaAdapter.setFirstOnly(true);
        return alphaAdapter;
    }
}
