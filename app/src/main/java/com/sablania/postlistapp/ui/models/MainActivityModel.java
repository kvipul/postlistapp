package com.sablania.postlistapp.ui.models;

import android.content.res.AssetManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sablania.postlistapp.models.PostModel;
import com.sablania.postlistapp.ui.contracts.MainActivityContract;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ViPul Sublaniya on 10-06-2018.
 */

public class MainActivityModel implements MainActivityContract.Model {
    @Override
    public List<PostModel> getPostData() {
        try {
            InputStream is = getClass().getClassLoader().getResourceAsStream("res/raw/posts.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String dataString = new String(buffer, "UTF-8");
            Type listType = new TypeToken<List<PostModel>>() {
            }.getType();
            return new Gson().fromJson(dataString, listType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
