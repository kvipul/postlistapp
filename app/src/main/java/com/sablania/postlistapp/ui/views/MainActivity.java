package com.sablania.postlistapp.ui.views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.sablania.postlistapp.R;
import com.sablania.postlistapp.models.PostModel;
import com.sablania.postlistapp.ui.adapters.PostsListAdapter;
import com.sablania.postlistapp.ui.contracts.MainActivityContract;
import com.sablania.postlistapp.ui.presenters.MainActivityPresenter;
import com.sablania.postlistapp.utils.Constants;

import java.util.List;

public class MainActivity extends AppCompatActivity implements MainActivityContract.View {
    RecyclerView recyclerView;
    MainActivityPresenter mainActivityPresenter;
    PostsListAdapter postsListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initializing presenter for main activity
        mainActivityPresenter = new MainActivityPresenter(this);

        //Asking data from presenter
        mainActivityPresenter.fetchPostData();
    }

    //when activity initializes its presenter this function will be called to initialize all the view in activity
    @Override
    public void initializeViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView = findViewById(R.id.recycler_view);
        postsListAdapter = new PostsListAdapter(this);

        //to add animation to adapter and set adapter to recycler view
        recyclerView.setAdapter(Constants.addAnimationToAdapter(postsListAdapter));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    //presenter will invoke this method whenever asked data will be ready for use
    @Override
    public void setViewData(List<PostModel> postModelList) {
        postsListAdapter.addPosts(postModelList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(this, "No Setting Found!", Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
