package com.sablania.postlistapp.ui.contracts;

import android.content.res.AssetManager;

import com.sablania.postlistapp.models.PostModel;

import java.util.List;

/**
 * Created by ViPul Sublaniya on 10-06-2018.
 */

public interface MainActivityContract {
    //For a single activity there will be a contract where we define view, presenter and model interfaces

    interface View{
        //activity will behave like a view itself
        //Note: Here view is referred to view in MVP no andoid view class
        void initializeViews();
        void setViewData(List<PostModel> postModelList);
    }
    interface Model{
        //there will be a model class for activity that will extend this interface and will provide data
        //to activity
        List<PostModel> getPostData();
    }
    interface Presenter{
        //finally presenter will connect model to view
        void fetchPostData();
    }
}
