package com.sablania.postlistapp.ui.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sablania.postlistapp.R;
import com.sablania.postlistapp.models.PostModel;
import com.sablania.postlistapp.models.TherapeuticsModel;
import com.sablania.postlistapp.utils.BackgroundSpan;
import com.sablania.postlistapp.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PostsListAdapter extends RecyclerView.Adapter<PostsListAdapter.ViewHolder> {

    private List<PostModel> posts;
    private Context context;

    public PostsListAdapter(Context context) {
        this.posts = new ArrayList<>();
        this.context = context;
    }

    public void addPosts(List<PostModel> newPosts) {
        posts.addAll(newPosts);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_item_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        PostModel post = posts.get(position);
        viewHolder.tvTitle.setText(post.getTitle());
        viewHolder.tvAuthor.setText(post.getUser().getAuthorName());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        try {
            viewHolder.tvCreationDate.setText(new SimpleDateFormat("HH:mm:ss\n dd-MM-yyyy").format(sdf.parse(post.getCreationDate())));
        } catch (ParseException e) {
            viewHolder.tvCreationDate.setText("");
            e.printStackTrace();
        }
        if (!post.getTherapeutics().isEmpty()) {
            SpannableStringBuilder styledString = new SpannableStringBuilder("");
            for (TherapeuticsModel tm : post.getTherapeutics()) {
                SpannableString ss = new SpannableString(" " + tm.getName() + " ");
                Resources rs = context.getResources();
                ss.setSpan(new BackgroundSpan(rs.getColor(R.color.colorPrimary), rs.getColor(android.R.color.white), 5, 5, 0, 10 ), 0, tm.getName().length()+1, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                styledString.append(ss);
            }
            viewHolder.tvTherapeutics.setText(styledString);
            viewHolder.tvTherapeutics.setVisibility(View.VISIBLE);
        } else {
            viewHolder.tvTherapeutics.setVisibility(View.GONE);
        }
        if(post.getUser().getThumbnailUrl().isEmpty()){
            viewHolder.ivThumbnail.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_profile_pic));
        }else {
            Picasso.with(context).load(post.getUser().getThumbnailUrl()).transform(new CircleTransform()).into(viewHolder.ivThumbnail);
        }
        if (post.getAttachments().isEmpty()) {
            viewHolder.ivAttachment.setVisibility(View.GONE);
        } else {
            viewHolder.ivAttachment.setVisibility(View.VISIBLE);
            Picasso.with(context).load(post.getAttachments().get(0).getUrl()).into(viewHolder.ivAttachment);
        }
        viewHolder.tvDescription.setText(post.getShortBodyText());
        viewHolder.tvViews.setText(post.getViewCount()+" Views");
        viewHolder.tvVotes.setText(post.getVoteCount()+" Votes");
        viewHolder.tvComments.setText(post.getAnswerCount()+" Comments");
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvAuthor, tvTherapeutics, tvViews, tvVotes, tvComments, tvDescription, tvCreationDate;
        ImageView ivThumbnail, ivAttachment;

        ViewHolder(View view) {
            super(view);
            tvTitle = view.findViewById(R.id.tv_title);
            tvAuthor = view.findViewById(R.id.tv_author_name);
            tvTherapeutics = view.findViewById(R.id.tv_therapeutics);
            tvViews = view.findViewById(R.id.tv_views);
            tvVotes = view.findViewById(R.id.tv_votes);
            tvDescription = view.findViewById(R.id.tv_description);
            tvComments = view.findViewById(R.id.tv_comments);
            ivThumbnail = view.findViewById(R.id.iv_thumbnail);
            ivAttachment = view.findViewById(R.id.iv_attachment);
            tvCreationDate = view.findViewById(R.id.tv_creation_date);
        }
    }

}
