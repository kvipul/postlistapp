package com.sablania.postlistapp.ui.presenters;

import android.content.res.AssetManager;

import com.sablania.postlistapp.models.PostModel;
import com.sablania.postlistapp.ui.contracts.MainActivityContract;
import com.sablania.postlistapp.ui.models.MainActivityModel;

import java.util.List;

/**
 * Created by ViPul Sublaniya on 10-06-2018.
 */

public class MainActivityPresenter implements MainActivityContract.Presenter {
    private MainActivityContract.View view;
    private MainActivityContract.Model model;

    public MainActivityPresenter(MainActivityContract.View view) {
        this.view = view;
        initPresenter();
    }

    private void initPresenter() {
        this.model = new MainActivityModel();
        view.initializeViews();
    }

    @Override
    public void fetchPostData() {
        //presenter will ask data from the model and pass to view(activity)
        List<PostModel> data = model.getPostData();
        view.setViewData(data);
    }
}
