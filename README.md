#PostListApp
Development Time: 10 Hrs

##Aim
PostListApp is an app that fetch Posts (json data) from a local project file and lists them on home page.

##App Features
- App supports both orientations and wide range of devices with different screen sizes
- Implemented using MVP architecture for better maintainability, testability and scalability
- Material vector icons are used to keep app size small and remove resolution problem in different screen density mobiles

##Third Party libraries and References
- Icon image source: Material icons
- [Picasso](http://square.github.io/picasso/) : To load internet image from URL
- [Recycler view animator](https://github.com/wasabeef/recyclerview-animators) : To animate recycler view items
- Gson: To parse json data into required format
- Stackoverflow: Some part of Utility classes BackgroundSpan and CircleTransform


